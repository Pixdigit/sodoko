extends Node
class_name Solver

const DRAW_SOLVER = false


static func is_solvable(field: Field) -> bool:
	var test_field := Field.new()
	test_field.cells = field.cells.duplicate()
	var solve_success := solve(test_field)
	return test_field.is_finished() and solve_success


#Single pass solve attempt
static func solve(field: Field) -> bool:
	var candidates := {}

	for pos in field.coords():
		candidates[pos] = _direct_candidate_elimination_solve(field, pos)
	candidates = _block_requirement_solve(field, candidates)
	candidates = _candidate_requirement_solve(field, candidates)

	# If there is a cell with only one candidate left
	# then that is the solution for that cell
	for pos in field.coords():
		if len(candidates[pos]) == 1:
			field.set_at(pos, candidates[pos][0])
			if field.is_violation_at(pos):
				field.set_at(pos, 0)
				return false
	return true


#Detects for every cell each number that wouldn't cause a violation
static func _direct_candidate_elimination_solve(field: Field, pos: Vector2) -> Array:
	var candidates: Array
	if field.get_at(pos) == 0:
		candidates = range(1, field.SIZE + 1)
		for block in field.blocks_at(pos):
			for pos_check in block:
				var value_check := field.get_at(pos_check)
				if value_check in candidates:
					candidates.erase(value_check)
	else:
		candidates = []
	return candidates


# If there is a block and there is only one candidate for a number
# then there can't be another number in that cell
static func _candidate_requirement_solve(field: Field, candidates: Dictionary) -> Dictionary:
	for block in field.exclusion_blocks:
		for num in range(1, field.SIZE + 1):
			var occurences := 0
			for pos in block:
				if num in candidates[pos]:
					occurences += 1
			if occurences == 1:
				for pos in block:
					if num in candidates[pos]:
						candidates[pos] = [num]

	return candidates

# Checks if due to the constrains of one block, a given candidate can only
# exist within an overlap with another block. Therefore in the other cells
# of the second block the given candiate can not exist.
static func _block_requirement_solve(field: Field, candidates: Dictionary) -> Dictionary:
	# Global search for each number and cell
	for block_1 in field.exclusion_blocks:
		for num in range(1, field.SIZE + 1):

			# Get all cells within the block where num is a candidate
			var num_sector := []
			for pos in block_1:
				if num in candidates[pos]:
					num_sector.append(pos)

			# See if another block completly contains given sector
			if len(num_sector) > 0:
				var blocks := []
				for block_2 in field.exclusion_blocks:
					var contains := true
					for num_pos in num_sector:
						if not num_pos in block_2:
							contains = false
					if contains and block_2 != block_1:
						blocks.append(block_2)

				# Erase candidates outside of overlap
				if len(blocks) > 0:
					for block_2 in blocks:
						for pos in block_2:
							if not pos in num_sector:
								candidates[pos].erase(num)
	return candidates


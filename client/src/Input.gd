extends Node

func _ready():
	pass


func _input(event: InputEvent):
		if event.is_action_pressed("exit"):
			get_tree().quit()
		if event.is_action_pressed("solve"):
			Solver.solve(get_node("/root/Main/Grid").grid.field)
		if event.is_action_pressed("generate_puzzle"):
			get_node("/root/Main/Grid").generate()


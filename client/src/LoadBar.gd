extends Panel

class_name LoadBar

export var title := "" setget set_title, get_title
export var sub_title := "" setget set_subtitle, get_subtitle
export(float, 0, 100, 0.01) var percent := 0.0 setget set_percent, get_percent

var _title_label: Label
var _subtitle_label: Label
var _progress_bar: ProgressBar

func _init() -> void:
	var v_box_container: VBoxContainer = VBoxContainer.new()
	v_box_container.name = "VBoxContainer"
	v_box_container.alignment = BoxContainer.ALIGN_CENTER
	v_box_container.anchor_right = 1
	v_box_container.anchor_bottom = 1

	var title_center_container: CenterContainer = CenterContainer.new()
	title_center_container.name = "CenterContainer"
	title_center_container.size_flags_vertical = title_center_container.SIZE_EXPAND_FILL

	_title_label = Label.new()
	_title_label.name = "Title"
	_title_label.text = title

	var progress_v_box_container: VBoxContainer = VBoxContainer.new()
	progress_v_box_container.name = "Progress"
	progress_v_box_container.size_flags_vertical = progress_v_box_container.SIZE_EXPAND_FILL

	_subtitle_label = Label.new()
	_subtitle_label.name = "Subtitle"
	_subtitle_label.text = sub_title

	_progress_bar = ProgressBar.new()
	_progress_bar.name = "ProgressBar"
	_progress_bar.allow_greater = true

	progress_v_box_container.add_child(_subtitle_label)
	progress_v_box_container.add_child(_progress_bar)
	title_center_container.add_child(_title_label)
	v_box_container.add_child(title_center_container)
	v_box_container.add_child(progress_v_box_container)
	self.add_child(v_box_container)
	self.name = "LoadBar"


func get_percent() -> float:
	return _progress_bar.value


func get_subtitle() -> String:
	return _subtitle_label.text


func get_title() -> String:
	return _title_label.text


func set_percent(perc: float) -> void:
	_progress_bar.value = perc
	percent = perc


func set_subtitle(new_title: String) -> void:
	_subtitle_label.text = new_title


func set_title(new_title: String) -> void:
	_title_label.text = new_title



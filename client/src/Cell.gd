extends Control
class_name Cell

export(int, 1, 9) var value:int = 0 setget set_value, get_value

signal input(new_value)


var selected: bool = false


func _init(size: Vector2, cell_border: Texture, number_font: DynamicFont):
	var borders := NinePatchRect.new()
	borders.name = "Borders"
	borders.margin_right = size.x
	borders.margin_bottom = size.y
	borders.rect_size = size
	borders.texture = cell_border
	borders.region_rect = Rect2(0, 0, 120, 120)
	borders.patch_margin_top = 7
	borders.patch_margin_left = 7
	borders.patch_margin_bottom = 7
	borders.patch_margin_right = 7

	var background := ColorRect.new()
	background.name = "Background"
	background.color = Color(1, 1, 1)
	background.rect_size = size
	background.margin_right = size.x
	background.margin_bottom = size.y
	background.visible = true

	var button := Button.new()
	button.name = "Button"
	button.text = str(value)
	button.toggle_mode = true
	button.action_mode = BaseButton.ACTION_MODE_BUTTON_RELEASE
	button.flat = true
	button.rect_size = size
	button.margin_right = size.x
	button.margin_bottom = size.y
	number_font.size = int(size.y * 0.7)
	button.set("custom_fonts/font", number_font)
	button.set("custom_colors/font_color", Color(0, 0, 0))
	button.set("custom_colors/font_color_hover", Color(0.3, 0.3, 0.3))
	button.set("custom_colors/font_color_pressed", Color(0.1, 0.1, 0.1))
	button.set("custom_colors/font_color_disabled", Color(0.1, 0.4, 0.1))
	var err := button.connect("button_down", self, "_on_click")
	assert(err == 0)

	self.add_child(background)
	self.add_child(borders)
	self.add_child(button)


func _input(event: InputEvent):
	if selected:
		if event is InputEventKey:
			var number := LargeNumber.str_to_int(char(event.unicode))
			if number != -1:
				emit_signal("input", number)


func _on_click():
	if not selected:
		get_parent().deselect_all()
		select()


func deselect() -> void:
	selected = false
	(get_node("./Background") as ColorRect).color = Color(1, 1, 1)
	(get_node("Button") as BaseButton).pressed = false


func disable() -> void:
	(get_node("./Button") as Button).disabled = true


func get_value() -> int:
	return value


func select() -> void:
	selected = true
	(get_node("./Background") as ColorRect).color = Color(0.7, 0.7, 0.7)
	(get_node("./Button") as BaseButton).pressed = true


func set_value(val: int) -> void:
	value = val
	if val != 0:
		(get_node("./Button") as Button).text = LargeNumber.int_to_str(val)
	else:
		(get_node("./Button") as Button).text = ""


func set_validity(valid: bool) -> void:
	var button: Button = get_node("./Button") as Button
	if valid:
		button.set("custom_colors/font_color", Color(0, 0, 0))
	else:
		button.set("custom_colors/font_color", Color(1, 0, 0))


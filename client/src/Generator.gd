extends Object
class_name Generator


static func _gen_structure() -> Control:
	var center_container: CenterContainer = CenterContainer.new()
	center_container.name = "Center"
	center_container.anchor_bottom = 1
	center_container.anchor_right = 1

	var dyna_grid: DynaGrid = DynaGrid.new()
	dyna_grid.name = "Cells"
	dyna_grid.cell_border = preload("res://assets/cellBorder.png")
	dyna_grid.number_font = preload("res://assets/Font.tres").duplicate(true)
	dyna_grid.margin_left = 420
	dyna_grid.margin_right = 1500
	dyna_grid.margin_bottom = 1080
	dyna_grid.rect_min_size = Vector2(1080, 1080)

	var err := center_container.connect("resized", dyna_grid, "_on_parent_resized")
	assert(err == 0)
	center_container.add_child(dyna_grid)
	return center_container


static func _step_gen(i: int, field: Field) -> int:
	var pos := Vector2(i % (field.COLUMS * field.ROWS), floor(float(i) / (field.COLUMS * field.ROWS)))

	if field.get_at(pos) == field.SIZE:
		field.set_at(pos, 0)
		i -= 1
	else:
		field.set_at(pos, field.get_at(pos) + 1)
		var is_violation := field.is_violation_at(pos)
		if not is_violation:
			i += 1
	return i


static func _update_progress(i: int, progress: LoadBar, field: Field) -> bool:
	var percent := 0.0
	var old_progress := progress.percent

	if i <= pow(field.ROWS * field.COLUMS, 2):
		var generate_percent = float(i) / (pow(field.ROWS * field.COLUMS, 2) - 1) * 100
		percent = generate_percent / 2

	progress.percent = max(progress.percent, percent)
	return progress.percent > old_progress


static func generate(parent: Container) -> void:
	var structure: Control = _gen_structure()
	parent.add_child(structure)
	var dyna_grid := structure.get_node("./Cells") as DynaGrid

	var load_bar := LoadBar.new()
	load_bar.anchor_right = 1
	load_bar.anchor_bottom = 1
	load_bar.title = "GENERATING_PUZZLE"
	load_bar.sub_title = "SEARCHING_SOLUTION"
	load_bar.visible = false
	parent.add_child(load_bar)

	#dyna_grid.cell_updates_enabled = false
	dyna_grid.field.clear()

	var i := 0
	while i != pow(dyna_grid.field.ROWS * dyna_grid.field.COLUMS, 2):
		var change = _update_progress(i, load_bar, dyna_grid.field)
		if change || true:
			yield(parent.get_tree(), "idle_frame")
		i = _step_gen(i, dyna_grid.field)

	#Scramble Sudoku
	load_bar.sub_title = "SCRAMBLE_SOLUTION"
	var keys := range(1, dyna_grid.field.SIZE + 1)
	keys.shuffle()
	var map := {}
	for i in range(len(keys)):
		map[keys[i]] = i + 1
		load_bar.percent = 50 + (float(i) / len(keys)) * 25
		yield(parent.get_tree(), "idle_frame")
	dyna_grid.field.mutate(map)
	load_bar.percent = 75
	yield(parent.get_tree(), "idle_frame")


	#Scramble Sudoku
	load_bar.sub_title = "REDUCE_SUDOKU"
	var removing_order := dyna_grid.field.coords()
	removing_order.shuffle()
	for i in range(len(removing_order)):
		var original_value := dyna_grid.field.get_at(removing_order[i])
		dyna_grid.field.set_at(removing_order[i], 0)
		if (not Solver.is_solvable(dyna_grid.field)):
			dyna_grid.field.set_at(removing_order[i], original_value)

		load_bar.percent = 75 + (float(i) / len(removing_order)) * 25
		yield(parent.get_tree(), "idle_frame")
	load_bar.percent = 100
	yield(parent.get_tree(), "idle_frame")

	dyna_grid.cell_updates_enabled = true
	dyna_grid.disable_cells(dyna_grid.field.occupied_coords())
	dyna_grid.update_cell_context()
	parent.remove_child(load_bar)


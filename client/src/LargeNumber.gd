extends Node
class_name LargeNumber

const ALPHABET = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]


static func int_to_str(num: int) -> String:
	var string: String
	if 0 <= num && num <= 9:
		string = str(num)
	elif 10 <= num && num < 10 + 26:
		string = ALPHABET[num - 10]
	elif 10 <= num && num < 10 + 26 * 2:
		string = (ALPHABET[num - 10 - 26] as String).to_upper()
	else:
		string = "?"
	return string


static func str_to_int(string: String) -> int:
	var num: int
	if string == "0":
		num = 0
	elif int(string) != 0:
		num = int(string)
	elif string.to_lower() in ALPHABET:
		if string.to_lower() == string:
			num = ALPHABET.find(string) + 10
		else:
			num = ALPHABET.find(string.to_lower()) + 10 + 26
	else:
		num = -1
	return num

extends Object
class_name Field

signal data_changed

const ROWS = 4
const COLUMS = 4
const SIZE = ROWS * COLUMS

# Turns out dicts are faster than arrays to access
var cells := {}
var exclusion_blocks := []


func _init():
	for y in range(SIZE):
		for x in range(SIZE):
			cells[Vector2(x, y)] = 0 # complicated_solve[y][x]

	for i in range(SIZE):
		#rows
		exclusion_blocks.append(_vector_space(Vector2(0, i), Vector2(SIZE, i + 1)))
		#colums
		exclusion_blocks.append(_vector_space(Vector2(i, 0), Vector2(i + 1, SIZE)))
		#rectangle blocks
		var block_topleft := Vector2((i % COLUMS) * ROWS, floor(float(i) / COLUMS) * COLUMS)
		exclusion_blocks.append(_vector_space(block_topleft, block_topleft + Vector2(ROWS, COLUMS)))


func _raw_get_at(pos: Vector2) -> int:
	return cells[pos]


func _valid_coord(pos: Vector2) -> bool:
	return pos in cells


func _vector_space(from: Vector2, to: Vector2) -> Array:
	var space := []
	for x in range(to.x - from.x):
		for y in range(to.y - from.y):
			space.append(Vector2(x + from.x, y + from.y))
	return space


func blocks_at(pos: Vector2) -> Array:
	var blocks := []
	for block in exclusion_blocks:
		if pos in block:
			blocks.append(block)
	return blocks


func clear() -> void:
	for pos in coords():
		self.set_at(pos, 0)


func coords() -> Array:
	return _vector_space(Vector2(0, 0), Vector2(SIZE, SIZE))


func get_at(pos: Vector2) -> int:
	assert(_valid_coord(pos))
	return _raw_get_at(pos)


func is_finished() -> bool:
	for pos in coords():
		if _raw_get_at(pos) == 0:
			return false
	return true


func is_violation_at(pos: Vector2) -> bool:
	assert(_valid_coord(pos))
	var value := _raw_get_at(pos)
	for block in blocks_at(pos):
		for pos_check in block:
			if value == _raw_get_at(pos_check) \
					and pos != pos_check:
				return true
	return false


func mutate(map: Dictionary) -> void:
	for pos in coords():
		set_at(pos, map[get_at(pos)])


func occupied_coords() -> Array:
	var occupied_coordinates := []
	for pos in coords():
		if get_at(pos) != 0:
			occupied_coordinates.append(pos)
	return occupied_coordinates


func set_at(pos: Vector2, value: int) -> void:
	assert(_valid_coord(pos))
	assert(value >= 0)
	if value <= SIZE:
		cells[pos] = value
		emit_signal("data_changed")
	else:
		print_debug("Silently ignored number (", value, ") input over limit")


func substitute(a: int, b: int) -> void:
	assert(a != b and 0 < a && a <= SIZE and 0 < b && b <= SIZE)
	for pos in coords():
		if _raw_get_at(pos) == a:
			set_at(pos, b)
		if _raw_get_at(pos) == b:
			set_at(pos, a)


func violations() -> Array:
	var violations := []
	for pos in coords():
		if is_violation_at(pos):
			violations.append(pos)
	return violations


func violations_at(pos: Vector2) -> Array:
	assert(_valid_coord(pos))
	var conflicts := []
	var value := _raw_get_at(pos)
	for block in blocks_at(pos):
		for pos_check in block:
			if value == _raw_get_at(pos_check) \
				and pos != pos_check:
				conflicts.append(pos_check)
	return conflicts


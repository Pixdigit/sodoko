extends Panel
class_name DynaGrid

export(Texture) var cell_border
export(DynamicFont) var number_font
export var editor_enable := false
export var auto_solve := false

var field: Field = Field.new()
var cell_updates_enabled := true
var state_desync := true


func _get_cell_at(pos: Vector2) -> Cell:
	return get_node("./Cell" + str(pos.x) + "|" + str(pos.y)) as Cell


func _gen_grid():
	for child in self.get_children():
		self.remove_child(child)
		child.queue_free()

	var size := Vector2(self.rect_size.x / field.SIZE,
			self.rect_size.y / field.SIZE)
	for pos in field.coords():
		var cell := Cell.new(size, cell_border, number_font)
		cell.rect_position = pos * size
		cell.name = "Cell" + str(pos.x) + "|" + str(pos.y)
		cell.value = field.get_at(pos)
		var err := cell.connect("input", self, "_on_cell_input", [pos])
		assert(err == 0)
		self.add_child(cell)

	var separator_size_factor: int
	if field.ROWS > field.COLUMS:
		separator_size_factor = (field.ROWS * 15)
	else:
		separator_size_factor = (field.COLUMS * 15)
	for i in range(field.COLUMS - 1):
		var v_sep = ColorRect.new()
		v_sep.name = "VSep" + str(i)
		v_sep.color = Color(0, 0, 0)
		v_sep.rect_size = Vector2(self.rect_size.x / separator_size_factor, self.rect_size.y)
		v_sep.rect_position = Vector2((i + 1) * self.rect_size.x / field.COLUMS - v_sep.rect_size.x / 2, 0)
		self.add_child(v_sep)
	for i in range(field.ROWS - 1):
		var h_sep = ColorRect.new()
		h_sep.name = "HSep" + str(i)
		h_sep.color = Color(0, 0, 0)
		h_sep.rect_size = Vector2(self.rect_size.x, self.rect_size.y / separator_size_factor)
		h_sep.rect_position = Vector2(0, (i + 1) * self.rect_size.y / field.ROWS - h_sep.rect_size.y / 2)
		self.add_child(h_sep)

	update_cell_context()


func _on_cell_input(value, pos):
	field.set_at(pos, value)
	deselect_all()
	update_cell_context()

	if auto_solve:
		self.cell_updates_enabled = false
		var field_changed := true
		while field_changed:
			var solve_success := Solver.solve(field)
			if not solve_success:
				break
			elif state_desync:
				update_cell_context()
			else:
				field_changed = false
		self.cell_updates_enabled = true


func _on_field_update():
	if cell_updates_enabled:
		update_cell_context()
	else:
		state_desync = true


func _on_parent_resized() -> void:
	var grandparent := get_parent().get_parent() as Control
	self.rect_min_size = Vector2(grandparent.rect_size.y, grandparent.rect_size.y)


func _process(delta):
	if Engine.editor_hint && editor_enable:
		_gen_grid()
		delta *= 1


func _ready() -> void:
	_gen_grid()
	var err := field.connect("data_changed", self, "_on_field_update")
	assert(err == 0)


func deselect_all() -> void:
	for node in get_children():
		if node is Cell:
			node.deselect()


func disable_cells(coords: Array) -> void:
	for pos in coords:
		var cell := _get_cell_at(pos)
		cell.disable()

func update_cell_context() -> void:
	state_desync = false
	for pos in field.coords():
		var cell := _get_cell_at(pos)
		cell.value = field._raw_get_at(pos)
		cell.set_validity(not field.is_violation_at(pos))

extends Node

const SERVER_IP = "localhost"
const SERVER_PORT = 1337

var peer = NetworkedMultiplayerENet.new()

func _ready():
	peer.create_client(SERVER_IP, SERVER_PORT)
	get_tree().set_network_peer(peer)

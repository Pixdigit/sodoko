extends Node

const SERVER_PORT = 1337
const MAX_PLAYERS = 2

var peer = NetworkedMultiplayerENet.new()
var lobbies = [Lobby.new()]

func _ready():
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().set_network_peer(peer)

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")

	print("Server started on port: ", SERVER_PORT)


func _player_connected(id):
	print("Player connected with id: ", id)

func _player_disconnected(id):
	print("Player disconnected with id: ", id)

remote func test():
	print("TEST")
